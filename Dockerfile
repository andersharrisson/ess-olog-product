FROM openjdk:11

ADD http://artifactory.esss.lu.se/artifactory/certificates/esss-ca01-ca.cer /usr/local/share/ca-certificates/esss-ca01-ca.crt
RUN update-ca-certificates

COPY target/ess-olog-product-*.jar /opt/olog/olog.jar

RUN useradd -ms /bin/bash olog
USER olog

CMD ["java", "-Djavax.net.ssl.trustStore=/usr/local/openjdk-11/lib/security/cacerts", "-jar", "/opt/olog/olog.jar"]
